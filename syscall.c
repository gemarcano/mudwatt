// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#include <sys/stat.h>
#include <errno.h>

// Defined in linker
extern unsigned char _sheap;

__attribute__((used))
void *_sbrk(int incr)
{
	static unsigned char *heap = &_sheap;
	unsigned char *prev = heap;
	heap += incr;
	return prev;
}

__attribute__((used))
void _exit(int rc)
{
	(void)rc;
	for(;;);
}

__attribute__((used))
int _getpid()
{
	return 1;
}

__attribute__((used))
int _fstat(int file, struct stat *st)
{
	(void)file;
	st->st_mode = S_IFCHR;
	return 0;
}

__attribute__((used))
int _kill(int pid, int sig)
{
	(void)pid;
	(void)sig;
	errno = EINVAL;
	return -1;
}

__attribute__((used))
int _close(int file)
{
	(void)file;
	errno = EBADF;
	return -1;
}

__attribute__((used))
int _isatty(int file)
{
	(void)file;
	return 1;
}

__attribute__((used))
int _read(int file, char *ptr, int len)
{
	(void)file;
	(void)ptr;
	(void)len;
	return 0;
}

__attribute__((used))
int _write(int file, char *buf, int nbytes)
{
	(void)file;
	(void)buf;
	(void)nbytes;
	return 0;
}

__attribute__((used))
int _lseek(int file, int offset, int whence)
{
	(void)file;
	(void)offset;
	(void)whence;
	return 0;
}
