// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#include "spi.h"
#include "font.h"
#include "eink.h"

#include "am_mcu_apollo.h"
#include "am_bsp.h"
#include "am_util.h"
#include <string.h>

#include <stdbool.h>
#include <stdlib.h>

void eink_init(struct eink *eink)
{
	eink->width = 64;
	eink->height = 96;
	eink->framebuffer = malloc(eink->width*eink->height/8);
	memset(eink->framebuffer, 0xFF, eink->width*eink->height/8);
}

void eink_poweron(struct eink *eink)
{
	(void)eink;
	am_hal_gpio_state_write(EINK_POWER, AM_HAL_GPIO_OUTPUT_SET);
	am_hal_gpio_pinconfig(EINK_POWER, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
	// As a note, there must be at least 1ms between power on and reset
	am_util_delay_ms(1);

	// Set lines to known states
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_SCL, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_CSB, AM_HAL_GPIO_OUTPUT_SET);
	spi_enable();

	// Begin reset. Reset must be low for at least 100us
	am_hal_gpio_state_write(SPI_BITBANG_RST, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_util_delay_us(200);
	am_hal_gpio_state_write(SPI_BITBANG_RST, AM_HAL_GPIO_OUTPUT_SET);
	// There must be at least 1 ms between RST going high and a command
	am_util_delay_ms(1);
	while(spi_busy());

	// PSR command, set resolution (not sure why there's also TRES)
	spi_send(0x00, (uint8_t[]){0xCF}, 1);
	// TRES command, set actual resolution. Actually sets last active source
	// (X) and gate (Y)
	// X formula is value * 8 - 1
	// Y formula is value - 1
	spi_send(0x61, (uint8_t[]){8 << 3, 96}, 2);

}

void eink_destroy(struct eink *eink)
{
	eink_shutdown(eink);

	free(eink->framebuffer);
	eink->framebuffer = NULL;
}

void eink_shutdown(struct eink *eink)
{
	(void)eink;
	// Send deep sleep (this... doesn't seem to do anything)
	spi_send(0x07, (uint8_t[]){0xA5}, 1);
	while(spi_busy());

	// Bring all pins down
	am_hal_gpio_state_write(SPI_BITBANG_RST, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_SDA, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_SCL, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_CSB, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(EINK_POWER, AM_HAL_GPIO_OUTPUT_CLEAR);
	spi_disable();
	// we need to keep the power pin active low (maybe a pulldown would be better?)
}

void eink_refresh(struct eink *eink)
{
	// DTM1 command, send old (?) data
	spi_send(0x10, NULL, 0);
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_SET); // Parameter
	for (size_t i = 0; i < 64*96/8; ++i)
	{
		spi_write(0xFF);
	}
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_CLEAR); // Parameter done

	spi_send(0x13, eink->framebuffer, eink->width*eink->height/8);

	// AUTO command, check 0xA5, does PON->DRF->POF
	// In english, this turns the display on, refreshes, then turns it off.
	// I'm not convinced turning it off does... anything.
	spi_send(0x17, (uint8_t[]){0xA5}, 1);
	while(spi_busy());
}
