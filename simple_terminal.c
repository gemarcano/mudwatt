// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#include "eink.h"
#include "font.h"
#include "simple_terminal.h"

#include <string.h>

void simple_terminal_init(struct simple_terminal *terminal, struct eink *eink)
{
	terminal->eink = eink;
	memset(eink->framebuffer, 0xFF, eink->width*eink->height/8);
	terminal->x_cursor = 0;
	terminal->y_cursor = 0;
	terminal->lines = eink->height/8;
	terminal->columns = eink->width/8;
}

void simple_terminal_print(struct simple_terminal *terminal, const char *str)
{
	unsigned char_width = terminal->eink->width/8;
	for (; *str; ++str)
	{
		for (size_t i = 0; i < 8; ++i)
			terminal->eink->framebuffer[terminal->y_cursor*char_width*8 + terminal->x_cursor + i*char_width] = ~font8x8_basic[(int)*str][i];
		terminal->x_cursor++;
		if (terminal->x_cursor >= terminal->columns)
		{
			terminal->y_cursor++;
			terminal->x_cursor = 0;
		}

		terminal->y_cursor %= terminal->lines;
	}
}

void simple_terminal_clear(struct simple_terminal *terminal)
{
	terminal->x_cursor = terminal->y_cursor = 0;
	memset(terminal->eink->framebuffer, 0xFF, terminal->eink->width*terminal->eink->height/8);
}
