# Software for Mudwatt harvesting experiment

## Setup

Follow the instructions on [how to set up the SDK from
Sparkfun](https://learn.sparkfun.com/tutorials/using-sparkfun-edge-board-with-ambiq-apollo3-sdk)
to finish configuring the [Sparkfun variant of the Ambiq Suite
SDK](https://github.com/sparkfun/AmbiqSuiteSDK). Once the SDK has been setup,
either move or clone this repository into the
`AmbiqSuiteSDK/boards_sfe/common/examples/` folder of the SDK.

## Building

```
BOARDPATH=../../../../redboard_artemis make
```

## Running

This assumes the Redboard Artemis still has its UART bridge. The appropriate
drivers should be installed (see the [Redboard Artemis hookup
guide](https://learn.sparkfun.com/tutorials/hookup-guide-for-the-sparkfun-redboard-artemis/all)).

The following command should flash the program to the Redboard over the UART
bridge:

```
COM_PORT=/dev/ttyUSB0 BOARDPATH=../../../../redboard_artemis make bootload
```

Modify `/dev/ttyUSB0` to the COM or tty device the Redboard's UART bridge
registers itself as on the system used to flash the program to the Redboard.

## Pins

Currently, the program uses the following pins on the Redboard:

| Redboard pin | e-ink pin | Description                    |
| ------------ | --------- | ------------------------------ |
| SCK          | SCL       | Clock for the e-ink display    |
| MISO         | DC        | Data/Control message indicator |
| MOSI         | SDA       | Data                           |
| 10           | CSB       | Chip select                    |
| 9            | BUSY_N    | Busy (active low)              |
| 8            | RST_N     | Reset (active low)             |
| 7            | N/A[^1]   | e-ink power enable             |

[^1]: This pin is connected to an external power switch that feeds power to the
  e-ink when high.
