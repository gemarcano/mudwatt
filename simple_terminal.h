// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#ifndef MUDWATT_SIMPLE_TERMINAL_H_
#define MUDWATT_SIMPLE_TERMINAL_H_

struct simple_terminal
{
	struct eink *eink;
	unsigned x_cursor;
	unsigned y_cursor;
	unsigned lines;
	unsigned columns;
};

void simple_terminal_init(struct simple_terminal *terminal, struct eink *eink);
void simple_terminal_print(struct simple_terminal *terminal, const char *str);
void simple_terminal_clear(struct simple_terminal *terminal);
#endif//MUDWATT_SIMPLE_TERMINAL_H_
