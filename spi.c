// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#include "spi.h"
#include "font.h"

#include "am_mcu_apollo.h"
#include "am_bsp.h"
#include "am_util.h"
#include <string.h>

#include <stdbool.h>

void spi_enable()
{
	am_hal_gpio_pinconfig(SPI_BITBANG_SDA, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
	am_hal_gpio_pinconfig(SPI_BITBANG_CSB, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
	am_hal_gpio_pinconfig(SPI_BITBANG_SCL, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
	am_hal_gpio_pinconfig(SPI_BITBANG_CSB, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
	am_hal_gpio_pinconfig(SPI_BITBANG_DC, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
	am_hal_gpio_pinconfig(SPI_BITBANG_BUSY, g_AM_HAL_GPIO_INPUT);
	am_hal_gpio_pinconfig(SPI_BITBANG_RST, g_AM_HAL_GPIO_OUTPUT_WITH_READ);
}

void spi_disable()
{
	am_hal_gpio_pinconfig(SPI_BITBANG_SDA, g_AM_HAL_GPIO_DISABLE);
	am_hal_gpio_pinconfig(SPI_BITBANG_CSB, g_AM_HAL_GPIO_DISABLE);
	am_hal_gpio_pinconfig(SPI_BITBANG_SCL, g_AM_HAL_GPIO_DISABLE);
	am_hal_gpio_pinconfig(SPI_BITBANG_CSB, g_AM_HAL_GPIO_DISABLE);
	am_hal_gpio_pinconfig(SPI_BITBANG_DC, g_AM_HAL_GPIO_DISABLE);
	am_hal_gpio_pinconfig(SPI_BITBANG_BUSY, g_AM_HAL_GPIO_DISABLE);
	am_hal_gpio_pinconfig(SPI_BITBANG_RST, g_AM_HAL_GPIO_DISABLE);
}

void spi_write(uint8_t value)
{
	am_hal_gpio_state_write(SPI_BITBANG_SCL, AM_HAL_GPIO_OUTPUT_CLEAR);
	am_hal_gpio_state_write(SPI_BITBANG_CSB, AM_HAL_GPIO_OUTPUT_CLEAR); // Initiate data transfer

	for (size_t i = 0; i < 8; ++i)
	{
		am_hal_gpio_state_write(SPI_BITBANG_SDA, (value & 0x80) ? AM_HAL_GPIO_OUTPUT_SET : AM_HAL_GPIO_OUTPUT_CLEAR);
		am_hal_gpio_state_write(SPI_BITBANG_SCL, AM_HAL_GPIO_OUTPUT_SET);
		am_util_delay_us(1);
		am_hal_gpio_state_write(SPI_BITBANG_SCL, AM_HAL_GPIO_OUTPUT_CLEAR);
		value <<= 1;
	}
	am_hal_gpio_state_write(SPI_BITBANG_CSB, AM_HAL_GPIO_OUTPUT_SET); // END data transfer
	am_hal_gpio_state_write(SPI_BITBANG_SDA, AM_HAL_GPIO_OUTPUT_CLEAR);
}

void spi_send(uint8_t command, uint8_t parameters[], size_t parameter_size)
{
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_CLEAR); // Command
	spi_write(command);
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_SET); // Parameter
	for (size_t i = 0; i < parameter_size; ++i)
	{
		spi_write(parameters[i]);
	}
	am_hal_gpio_state_write(SPI_BITBANG_DC, AM_HAL_GPIO_OUTPUT_CLEAR); // Command
}

bool spi_busy()
{
	uint32_t busy = 0;
	am_hal_gpio_state_read(SPI_BITBANG_BUSY, AM_HAL_GPIO_INPUT_READ, &busy);
	return !busy;
}

