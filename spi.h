// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#ifndef MUDWATT_SPI_H_
#define MUDWATT_SPI_H_

#include "am_mcu_apollo.h"

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define SPI_BITBANG_SDA AM_BSP_PRIM_SPI_SDO_PIN
#define SPI_BITBANG_SCL AM_BSP_PRIM_SPI_CLK_PIN
#define SPI_BITBANG_CSB 13
#define SPI_BITBANG_DC AM_BSP_PRIM_SPI_SDI_PIN
#define SPI_BITBANG_BUSY 12
#define SPI_BITBANG_RST 32

void spi_enable();
void spi_disable();
void spi_write(uint8_t value);
void spi_send(uint8_t command, uint8_t parameters[], size_t parameter_size);
bool spi_busy();

#endif//MUDWATT_SPI_H_
