// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#include "spi.h"
#include "eink.h"
#include "font.h"
#include "simple_terminal.h"

#include "am_mcu_apollo.h"
#include "am_bsp.h"
#include "am_util.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// FIXME move this to some RTC manager?
static const char *weekday[] =
{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Invalid"
};

static const char *month[] =
{
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December",
	"Invalid"
};

static int month_index(const char *str)
{
	for (size_t i = 0; i < 12; i++)
		if (strncmp(month[i], str, 3) == 0)
			return i;
	return 12;
}

static am_hal_ctimer_config_t timer_config =
{
	// Don't link timers.
	.ui32Link = 0,

	// Set up TimerA0.
	.ui32TimerAConfig = (
		AM_HAL_CTIMER_FN_REPEAT |
		AM_HAL_CTIMER_INT_ENABLE |
		AM_HAL_CTIMER_LFRC_32HZ
	),

	// No configuration required for TimerB0.
	.ui32TimerBConfig = 0,
};

static volatile am_hal_rtc_time_t hal_time = {0};

static void timerA0_init(void)
{
	am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_LFRC_START, 0);
	am_hal_ctimer_clear(0, AM_HAL_CTIMER_TIMERA);
	am_hal_ctimer_config(0, &timer_config);
	am_hal_ctimer_period_set(0, AM_HAL_CTIMER_TIMERA, (32*20)-1, 0);
	am_hal_ctimer_int_clear(AM_HAL_CTIMER_INT_TIMERA0);
}

// This is a weak symbol we're overriding
void am_ctimer_isr(void)
{
	am_hal_ctimer_int_clear(AM_HAL_CTIMER_INT_TIMERA0);

	// And while we're at it, grab the RTC time
	am_hal_rtc_time_t hal_time_;
	am_hal_rtc_time_get(&hal_time_);
	hal_time = hal_time_;
}

#define CHECK_ERRORS(x) \
	if ((x) != AM_HAL_STATUS_SUCCESS) \
	{ \
		error_handler(x); \
	}

// Catch HAL errors.
static void error_handler(uint32_t error)
{
	(void)error;
	for(;;)
	{
		am_devices_led_on(am_bsp_psLEDs, 0);
		am_util_delay_ms(500);
		am_devices_led_off(am_bsp_psLEDs, 0);
		am_util_delay_ms(500);
	}
}

int main(void)
{
	// Do basic MCU init
	am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_SYSCLK_MAX, 0);

	am_hal_cachectrl_config(&am_hal_cachectrl_defaults);
	am_hal_cachectrl_enable();

	am_bsp_low_power_init();

	// FIXME stuff these into some RTC object
	// Enable the XT for the RTC.
	am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_XTAL_START, 0);

	// Select XT for RTC clock source
	am_hal_rtc_osc_select(AM_HAL_RTC_OSC_XT);

	// Enable the RTC.
	am_hal_rtc_osc_enable();

	// Set the RTC time based on build-time time and date
	hal_time.ui32Hour = atoi(&__TIME__[0]);
	hal_time.ui32Minute = atoi(&__TIME__[3]);
	hal_time.ui32Second = atoi(&__TIME__[6]);
	hal_time.ui32Hundredths = 0x00;
	hal_time.ui32Weekday = am_util_time_computeDayofWeek(2000 + atoi(&__DATE__[9]), month_index(&__DATE__[0]) + 1, atoi(&__DATE__[4]) );
	hal_time.ui32DayOfMonth = atoi(&__DATE__[4]);
	hal_time.ui32Month = month_index(&__DATE__[0]);
	hal_time.ui32Year = atoi(&__DATE__[9]);
	hal_time.ui32Century = 0; // FIXME can we figure out what this is?

	// TimerA0 init. For wakeup purposes.
	timerA0_init();

	// Enable the timer Interrupt.
	am_hal_ctimer_int_enable(AM_HAL_CTIMER_INT_TIMERA0);

	// Enable the timer and uart interrupts in the NVIC.
	NVIC_EnableIRQ(CTIMER_IRQn);
	am_hal_interrupt_master_enable();

	// Enable the timer.
	am_hal_ctimer_start(0, AM_HAL_CTIMER_TIMERA);

	am_hal_rtc_time_t hal_time_ = hal_time;
	am_hal_rtc_time_set(&hal_time_);
	am_hal_rtc_time_get(&hal_time_);
	struct eink eink;
	eink_init(&eink);
	eink_poweron(&eink);
	struct simple_terminal terminal;
	simple_terminal_init(&terminal, &eink);

	for(;;)
	{
		// Grab the RTC time, as updated by the timer isr
		hal_time_ = hal_time;
		char buf[10];
		terminal.y_cursor = terminal.x_cursor = 0;
		snprintf(buf, 10, "%02lu:%02lu:%02lu", hal_time_.ui32Hour, hal_time_.ui32Minute, hal_time_.ui32Second);
		simple_terminal_clear(&terminal);
		simple_terminal_print(&terminal, buf);
		eink_poweron(&eink);
		eink_refresh(&eink);
		eink_shutdown(&eink);

		// Go to Deep Sleep and wait for a wake up.
		am_hal_sysctrl_sleep(AM_HAL_SYSCTRL_SLEEP_DEEP);
	}
}
