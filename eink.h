// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// SPDX-FileCopyrightText: 2021 Gabriel Marcano

#ifndef MUDWATT_EINK_H_
#define MUDWATT_EINK_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define EINK_POWER 28

struct eink
{
	unsigned width, height;
	unsigned char *framebuffer;
};

void eink_init(struct eink *eink);
void eink_destroy(struct eink *eink);
void eink_poweron(struct eink *eink);
void eink_shutdown(struct eink *eink);
void eink_refresh(struct eink *eink);

#endif//MUDWATT_EINK_H_
